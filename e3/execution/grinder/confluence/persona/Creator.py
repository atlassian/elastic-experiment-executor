from confluence.common.CommonActions import *
from confluence.common.MacroHelper import *
from confluence.common.helper.Authentication import login, logout
from confluence.common.helper.ConfluenceUserCreator import *
from confluence.common.helper.Utils import *
from confluence.common.helper.SynchronyUtils import *
from confluence.common.wrapper.User import User

from TestScript import TestScript

from net.grinder.script.Grinder import grinder
from synchrony.loadtest.java.interaction import BasicInteraction


class Creator(TestScript):
    EDITING_SESSION_DURATION = 10000L

    def __init__(self, number, args):
        super(Creator, self).__init__(number, args)
        self._current_user = None

        self._sample_page_text = get_page_sample_text()
        self.last_source_page_title = ''
        self.init_page_source_label = 'resource_page'
        self.page_source_label = self.init_page_source_label
        self.num_page_source_per_report = 5
        self.num_page_source = 0

    def __call__(self, *args, **kwargs):
        self._user_name = get_user_name()
        self._current_user = User(self._user_name, self._user_name)

        grinder.logger.info("Starting Creator persona with user name %s" % self._user_name)
        safe_execute(self, self.logger, self._test, self.report_success)

    def _test(self):
        """
        Simulate user who create page with page properties report macro
        and excerpt include macro.
        For every run match with is_running_special_action then it will create page
        which have page properties macro and excerpt macro
        :return:
        """
        rand_str = lambda n: ''.join([random.choice(string.lowercase) for i in xrange(n)])
        space_key = "ds"
        run_num = grinder.getRunNumber()
        macros_on_page = []

        login(self, self._current_user)

        # Only add source page every running 10 times
        if is_running_special_action(self, run_num):
            grinder.logger.info("Creating source page. Using label for page source: %s" % self.page_source_label)
            # adding source page for page properties and excerpt
            page_title = "Creator page properties and excerpt %s" % rand_str(15)
            is_adding_report_macros = self._adding_page_source(page_title, space_key)
            if is_adding_report_macros:
                page_response = self.http("GET", "/display/%s/%s" % (space_key, url_encode(page_title)))
                source_page_id = get_meta_attribute(page_response, 'ajs-page-id')
                label_form = [
                    {
                        "name": self.page_source_label
                    }
                ]
                self.rest("POST", '/rest/ui/1.0/content/%s/labels' % source_page_id, label_form)
                macros_on_page = ['details', 'excerpt']
                self.last_source_page_title = page_title
                self.num_page_source += 1
                if not self.num_page_source % self.num_page_source_per_report:
                    # change page source label if we reach the limit page in report
                    grinder.logger.info("%s" % self.num_page_source)
                    self.page_source_label = "%s-%d" % (self.init_page_source_label,
                                                        self.num_page_source / self.num_page_source_per_report)
        else:
            grinder.logger.info("Creating report page")
            # adding report page for page properties report and excerpt include
            page_title = "Creator page %s" % rand_str(15)
            random_page_index = random.randint(0, len(self._sample_page_text) - 1)
            _last_source_page_title = self.last_source_page_title

            # inline method to adding macros into page
            def adding_macros(page_id, new_page_body):
                random_macros_result = get_random_macro_editor_format(
                    self,
                    page_id,
                    defaultdict(str, report_label=self.page_source_label,
                                source_page_title=_last_source_page_title),
                    macro_browser_callback
                )
                new_page_body += random_macros_result['body']
                macros_on_page.extend(random_macros_result['macros'])
                return new_page_body

            self._create_page(space_key, page_title, random_page_index, adding_macros)

        grinder.logger.info("%s" % macros_on_page)
        view_page_by_page_url(self, "/display/%s/%s" % (space_key, url_encode(page_title)), macro_list=macros_on_page)

        is_success = is_http_ok()

        logout(self)
        return is_success

    def _adding_page_source(self, page_title, space_key):
        def adding_source_macros(page_id, new_page_body):
            macro_list = ['details', 'excerpt']
            for macro in macro_list:
                editor_macro_review(self, page_id, macro, '')
                editor_macro_placeholder(self, page_id, macro, '')
                new_page_body += get_macro_editor_format(self, self.test_data.base_url, macro)
            return new_page_body

        self._create_page(space_key,
                          page_title,
                          -1,
                          adding_source_macros)
        return is_http_ok()

    def _create_page(self, space_key, page_title, random_page_index,
                     body_creation_callback=None, after_page_creation_callback=None):
        response = self.http("GET", "/pages/createpage.action?spaceKey=%s" % space_key)
        page_id = get_meta_attribute(response, 'ajs-draft-id')
        new_page_body = '' if random_page_index < 0 else self._sample_page_text[random_page_index]["pageText"]

        # ########### Getting synchrony data ###########
        synchrony_jwt_token = get_meta_attribute(response, 'ajs-synchrony-token')
        content_id = get_meta_attribute(response, 'ajs-content-id')
        synchrony_application_id = get_meta_attribute(response, 'ajs-synchrony-app-id')
        synchrony_base_url = get_meta_attribute(response, 'ajs-synchrony-base-url')
        if not validate_synchrony_meta(synchrony_jwt_token, content_id, synchrony_application_id,
                                       synchrony_base_url):
            grinder.logger.error("Synchrony metadata is not provided: token=%s content_id=%s application_id=%s "
                                 "synchrony_base_url=%s" %
                                 (synchrony_jwt_token, content_id, synchrony_application_id, synchrony_base_url))
            self.report_success(False)
            return

        # ########### Getting draft data ###########
        draft_type = get_meta_attribute(response, 'ajs-draft-type')
        sync_rev = get_input_value(response, 'syncRev')
        if not validate_drafts_data(page_id, draft_type, sync_rev):
            grinder.logger.error("Drafts metadata is not provided: draft_id=%s draft_type=%s "
                                 "sync_rev=%s" % (page_id, draft_type, sync_rev))
            self.report_success(False)
            return

        # ########### Editing page ###########
        synchrony_session = BasicInteraction()
        synchrony_options = create_synchrony_options(synchrony_jwt_token, content_id,
                                                     synchrony_application_id,
                                                     parse_synchrony_url(synchrony_base_url))

        # Page editing
        synchrony_session.run(synchrony_options, self.EDITING_SESSION_DURATION)

        # Interactions with macros browser
        editor_macro_browser(self)
        editor_save_draft(self, page_id, 0, new_page_body, page_title, space_key)

        if body_creation_callback:
            new_page_body += body_creation_callback(page_id, new_page_body)

        # Page editing - another session
        synchrony_session.run(synchrony_options, self.EDITING_SESSION_DURATION)
        editor_save_draft(self, page_id, 0, new_page_body, page_title, space_key, sync_rev)
        # ####################################

        # ########### Saving a page ###########
        editor_adding_page(self,
                           0,
                           new_page_body,
                           page_id,
                           space_key,
                           page_title,
                           '1',
                           sync_rev)

        if after_page_creation_callback:
            after_page_creation_callback(page_id)
